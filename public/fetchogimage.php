<?php
//require composer autoload file
require(__DIR__ . '/../vendor/autoload.php');

use Embed\Embed;

// Database configuration
$dbConfig =  require(__DIR__ . '/../config/db.php');

$target_url = filter_input(INPUT_GET, 'target_url');

header('Content-type: application/json');
if($target_url){

try{

//Get the info
$info = Embed::create($target_url);

//Get all providers
$providers = $info->getProviders();

//Get the opengraph provider
$opengraph = $providers['opengraph'];


// get og image urls
$ogImageUrls = $opengraph->getImagesUrls();

if(array_key_exists(0,$ogImageUrls)){

        $dbh = new PDO($dbConfig['connection'], $dbConfig['username'], $dbConfig['password']);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $dbh->prepare("INSERT INTO og_images (url,image,created_at) VALUES (:url, :image, :created_at)");
        $stmt->bindParam(':url', $url);
        $stmt->bindParam(':image', $image);
        $stmt->bindParam(':created_at',$created_at);

        // insert data to og_images table row
        $url = $target_url;
        $image = $ogImageUrls[0];

        //set default timezone to UTC
        date_default_timezone_set('UTC');
        $created_at = date("Y-m-d H:i:s");

        $stmt->execute();

        echo json_encode(['success'=>true,'data'=>['url'=>$target_url,'image'=>$image,'created_at'=>$created_at]]);
}else{

    echo json_encode(['success'=>false,'message'=>'Target url does not have og:image meta tag']);
}

}catch(Exception $e)
{
    echo json_encode(['success'=>false,'message'=>"Error: " . $e->getMessage()]);
}


}else{

    echo json_encode(['success'=>false,'message'=>'Target url is required']);

}