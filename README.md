# Fetch og:image from a url

This project uses PHP, MySQL,  [composer](https://getcomposer.org) for dependencies manager, [embed/embed](https://packagist.org/packages/embed/embed) for fetching opengraph data from url, [phinx](https://phinx.org) for database migration, and [codeception](http://codeception.com) for testing.

Requirements :
 
- php 5.5+

# Setup & Installation


1.Download or clone this project


```
#!bash

 git clone https://ydatech@bitbucket.org/ydatech/fetch-ogimage-from-url.git
```

2.Instal Dependencies (I assume that you have already installed composer on your computer)
        
```
#!bash

composer install
```


3.Create MySQL Database with name development_db

4.Set Up database configuration (host,username,password,etc) in config/db.php and phinx.yml

-config/db.php for application connection
    
```
#!php

<?php

        return [
            "connection"=>"mysql:host=localhost;dbname=development_db;charset=utf8mb4",
            "username"=>"root",
            "password"=>""
        ];

```

-phinx.yml for database migration connection

 
```
#!yml

   development:
        adapter: mysql
        host: localhost
        name: development_db
        user: root
        pass: ''
        port: 3306
        charset: utf8
   
```


5.Run database migration

```
#!bash
        composer exec phinx migrate
            #or
        php ./vendor/bin/phinx migrate
            #or
        ./vendor/bin/phinx migrate
```


6.Run development server

```
#!bash

    php -S localhost:8000 -t public/

```





# RESTful Endpoint

Base url for development server is `http://localhost:8000`

1.`GET /fetchogimage.php`

#### Request paramters:
Parameter Name | Type | Description
---------|----------|---------
target_url (required)| string (URL)  | A target url, for example: https://sociocaster.com
    
#### Response data:

Name | Type | Description
---------|----------|---------
success | Boolean | Wheter the request is error or not 
data | Object | The data object if request is successful
data.url | String | Target Url
data.image | String | Open Graph Image Url
data.created_at | String (datetime)| Created date and time
message | String | Error message if request is failed    

#### Sample Request with curl
 
```
#!bash

      curl http://localhost:8000/fetchogimage.php?target_url=https://sociocaster.com
   
```


#### Sample JSON Responses:

* If Target URL valid and has og:image
               
```
#!json

          {
                "success": true,
                "data" : {
                    "url" : "https://sociocaster.com",
                    "image" : "https://sociocaster.com/upload/sociocasterliveshare.png",
                    "created_at" : "2017-03-06 08:19:13"
                }
          }
```

    
* If targate URL is invalid or does not have og:image
        
       
```
#!json

        {
            "success": false,
            "message" : "Error message"
        }
       
```




# Run Testing

As mentioned above that we use codeception to automated test the RESTful endpoint.


```
#!bash
        composer exec codecept run api
            #or
        php ./vendor/bin/codecept run api
            #or
        ./vendor/bin/codecept run api
```