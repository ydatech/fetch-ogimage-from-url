<?php


class OGFetcherCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // test fetch og:image from a url 
    public function fetchOGImage(ApiTester $I)
    {
        $I->wantTo('Fetch OG Image from a url');
        $I->sendGet('/fetchogimage.php',['target_url'=>'https://sociocaster.com']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(["success"=>true]);
        $I->seeResponseJsonMatchesJsonPath('$.data.image');
    }
    // test fetch image with invalid domain
    public function fetchOGImageWithInvalidUrl(ApiTester $I)
    {
        $I->wantTo('Fetch OG Image from an invalid url');
        $I->sendGet('/fetchogimage.php',['target_url'=>'https://invaliddomainte.com']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(["success"=>false]);
        $I->seeResponseJsonMatchesJsonPath('$.message');
    }

    // test fetch og:image from a url without og:image

    public function fetchOGImageWithoutOGImage(ApiTester $I)
    {
        $I->wantTo('Fetch OG Image from a url without og:image');
        $I->sendGet('/fetchogimage.php',['target_url'=>'http://php.net/manual/en/']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
         $I->seeResponseJsonMatchesJsonPath('$.success');
         $I->seeResponseJsonMatchesJsonPath('$.message');
        $I->seeResponseContainsJson(["success"=>false]);
        $I->seeResponseContainsJson(["message"=>"Target url does not have og:image meta tag"]);
       
    }
}
