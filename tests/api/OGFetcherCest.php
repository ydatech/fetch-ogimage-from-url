<?php


class OGFetcherCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function fetchOGImage(ApiTester $I)
    {
        $I->wantTo('Fetch OG Image from a url');
        $I->sendGet('/fetchogimage.php',['url'=>'preview']);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(["success"=>true]);
         $I->seeResponseJsonMatchesJsonPath('$.data.image');
    }
}
